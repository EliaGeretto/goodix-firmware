# Goodix Firmware

Firmwares for Goodix fingerprint readers.

Those firmwares are Goodix property.

They must remain hosted at `https://gitlab.freedesktop.org/mpi3d/goodix-firmware` only.

Please consider using `git submodule add https://gitlab.freedesktop.org/mpi3d/goodix-firmware.git` for adding them in your repo.

But don't add them directly in your repo by coping them.
